Installation
============


Installating tkwant from source
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Tkwant is a Python package that includes Cython modules.

At this point, tkwant must be installed from source (`official tkwant git repository <https://gitlab.kwant-project.org/kwant/tkwant>`_).  The `installation instructions of Kwant <https://kwant-project.org/doc/1/pre/install>`_ apply mostly also to tkwant, but in many cases installation will be as simple as::

    pip3 install git+https://gitlab.kwant-project.org/kwant/tkwant.git

Building tkwant for development
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Another approach, that is advantageous for development,
is to first clone the tkwant repository::

    git clone https://gitlab.kwant-project.org/kwant/tkwant.git

Then, after *cd* into the local repository, 
one can locally build tkwant with the command::

    python3 setup.py build_ext -i    

and make a symlink to the *tkwant* folder that is in the local repository.

Requirements
^^^^^^^^^^^^
Tkwant requires at least Python 3.4.
Several other libraries must be installed in order to build or to install tkwant. 
To miminize the effort, it is advantageous to install the required libraries below in the given order, that is
first *Kwant*, then *kwant-spectrum*, and then finally the *additional packages*.

Kwant
~~~~~

Follow the instructions for `installing Kwant <https://kwant-project.org/install>`_.

Kwant-Spectrum
~~~~~~~~~~~~~~

Kwant-Spectrum is a small pure-python library that is available under
https://gitlab.kwant-project.org/kwant/spectrum.
It can be installed via::

    pip3 install git+https://gitlab.kwant-project.org/kwant/spectrum.git


Additional packages
~~~~~~~~~~~~~~~~~~~

Tkwant needs several additional Python packages:

  ``mpi4py, dill, sympy``

The packages can be installed by standard *pip3 install* command.


Testing requirements
^^^^^^^^^^^^^^^^^^^^
The tkwant test suite needs several Python packages to run:

  ``pytest, pytest-cov, pytest-flakes, pytest-pep8``

The packages can be installed by standard *pip3 install* command.

Documentation requirements
^^^^^^^^^^^^^^^^^^^^^^^^^^
The tkwant test suite needs several Python packages to run:

  ``sphinx, jupyter-sphinx``

The packages can be installed by standard *pip3 install* command.
