## Important notice

This repository is deprecated and is still publicy available to showcase the tests performed to validate the energy transport calculation implementation for the legacy `args` kwant/tkwant approach in [this branch](https://gitlab.kwant-project.org/spec-gmt/tkwant-energy-transport/-/tree/validation-args/validation-scripts) and the new `params` kwant/tkwant approach in [this branch](https://gitlab.kwant-project.org/spec-gmt/tkwant-energy-transport/-/tree/validation-params/validation-scripts)

The official energy transport calculations code for tKwant has been implemented as a standalone module called `tkwantoperator`: more information is available in its [code repository](https://gitlab.kwant-project.org/kwant/tkwantoperator)

----------------------------------------

## Install instructions

The requirements come from kwant and tkwant. Install instructions and dependencies on Kwant [are given here](https://kwant-project.org/doc/1/pre/install), they have been used to write a more thorough guide in what follows.

### Packages for Ubuntu

1. Enable the "universe" and "multiverse" repositories: open an application called "Software & updates". In "Ubuntu Software" tab, check the the repos corresponding to "universe" and "multiverse".

2. Install some additional packages 
    ```shell
    sudo apt install git cython3 g++ g++-7  libhwloc-dev libibverbs-dev libjs-jquery-ui libblas-dev libmumps-scotch-5.1.2 libnuma-dev libscotch-6.0 libstdc++-7-dev g++ gfortran libmumps-scotch-dev build-essential libopenblas-dev liblapack-dev librsb-dev
    ```

3. Install python dependencies
    ```shell
    sudo apt install python3-pip ipython3 python3-mpi4py python3-dill python3-scipy python3-dev python3-setuptools python3-matplotlib python3-pytest python3-sympy python-matplotlib-data python3-cycler python3-dateutil python3-decorator python3-ipython python3-ipython-genutils python3-mpmath python3-pickleshare python3-prompt-toolkit python3-pygments python3-pyparsing python3-simplegeneric  python3-traitlets python3-wcwidth
    ```

4. Install python virtualenv to be able to have python virtual environments:
    ```shell
    sudo apt install python3-virtualenv virtualenv
    ```

### Packages for Fedora

The same tools and libraries need to be installed, but they have different names.

1. Additionnal packages
    ```shell
    sudo dnf install scotch scotch-devel MUMPS-openmpi-devel MUMPS-openmpi MUMPS MUMPS-devel MUMPS-mpich mpich mpich-devel metis metis-devel openblas* metis64 metis64-devel git gcc gcc-c++ gcc-gfortran lapack-static lapack-devel lapack64-static lapack64-devel
    ```

2. Python packages
    ```shell
    sudo dnf install python3-pip python3-dill python3-Cython python3-devel python3-scipy python3-setuptools python3-matplotlib python3-pytest python3-sympy python3-cycler python3-dateutil python3-decorator python3-ipython python3-matplotlib python3-mpmath python3-pickleshare python3-pygments python3-pyparsing python3-simplegeneric python3-sympy python3-traitlets python3-wcwidth python3-configparser python3-mpi4py-mpich python3-mpi4py-openmpi
    ```

4. Install python virtualenv to be able to have python virtual environments:
    ```shell
    sudo dnf install python3-virtualenv python3-virtualenv-api python3-virtualenv-clone python3-virtualenvwrapper python3-pytest-virtualenv
    ```

### Install instructions for Linux systems


1. **Optional: Use a virtual environment** Create a folder that will contain all the virtual environment folders, then create a virtual environment in it (a folder that will contain all the pip3 installs) and finally activate it (it will change the paths so that pip3 will install packages in it and you use its python3 executable):
    ```shell
    $ mkdir envs
    $ cd envs
    $ virtualenv --system-site-packages tkwant-env
    $ source envs/kwant-env/bin/activate
    ```
    _Note_: the last line "activates" the virtual environment. All pip installs will be made in it. Deactivating the virtual environment simply amounts to write
    ```shell
    $ deactivate
    ```
    Removing that virtual environment is done by deleting the `tkwant-env` folder.

2. **Fedora specific:** Load openmpi environment
    ```shell
      $ module load mpi/openmpi-x86_64 # can be mpi/mpich-x86_64
      $ export CPATH=/usr/include/openmpi-x86_64/ # can be /usr/include/mpich-x86_64/ should be consistent with the above line
    ```

3. Install latest kwant, kwant spectrum then tkwant:
    ```shell
      $ pip3 install git+https://gitlab.kwant-project.org/kwant/kwant.git
      $ pip3 install git+https://gitlab.kwant-project.org/kwant/spectrum.git
      $ pip3 install git+https://gitlab.kwant-project.org/spec-gmt/tkwant-energy-transport.git@energy-params
    ``` 
    _Note_: If the git branch `energy-params` gets updated. To install the changes, simply redo the last line

### Running scripts

You should be able to use kwant and tkwant now in the command line in that virtual environment. For example one can run the example `quantum_dot.py` from the `doc/examples` folder:

```shell
python3 doc/examples/quantum_dot.py
```

#### Parallel calculations

To make the calculation faster, it is possible to run scripts on several parallel cores. The script in the `examples` folder can for example be run in parallel on 10 cores:

```shell
mpirun -n 10 -bind-to none python3 doc/examples/quantum_dot.py
```

The `-bind-to none` option is there to not force the OS to use specific threads to run the 10 copies. If not used, the code will run on the first 10 threads of the machine. And if another script is ran in parallel using the same command, the same cores will be used for the second script, which will make the simulation slower, since the same cores are used for both simulations. There's also the possibility of choosing the threads on which to run the script

```shell
mpirun -n 5 --cpu-set 0-4 --bind-to core python3 doc/examples/quantum_dot.py
```

Where threads 0,1,2,3,4 will be used for the calculations, the same command can be written:

```shell
mpirun -n 5 --cpu-set 0,1,2,3,4 --bind-to core python3 doc/examples/quantum_dot.py
```
