.. _getting_started:

Getting started
===============

Tkwant is a python library to simulate time dependent transport phenomena in
tight-binding models. It is the time dependent generalization of the kwant package.

To show the usage of tkwant, we like to study the electron dynamics on an infinitly long
one-dimensional system of equally spaced scattering centers as a first example. 
The different scattering sites are coupled via nearest-neighbour couplings.
Being initially at thermal equilibrium, we are intersted in the electron
dynamics if a time-dependent perturbation :math:`V(t)` acts on one site
of the system. The theoretical background will not be covered in the
following. We refer to the literature (e.g. Refs. `[1] <#references>`__
and references therein) and the next tutorial section.

In order to numerically study this problem, the infinite chain is
subdevided into three pieces, a finite scattering region (:math:`S`)
consisting of :math:`N` distinct scattering sites, and two semi-infite
leads (:math:`L`), connected on the left and the right of the finite
piece. The Hamiltonian can be written as

.. math::


       \hat{H}(t) =  \sum_{i,j \in S} H_{ij}(t) \, \hat{c}^\dagger_i \hat{c}_j + \sum_{i,j \in L} H_{ij} \, \hat{c}^\dagger_i \hat{c}_j + \sum_{i \in S, j \in L} H_{ij} \, \hat{c}^\dagger_i \hat{c}_j + h.c. .

where :math:`\hat{c}^\dagger_i` (:math:`\hat{c}_j`) is a creation
(destruction) operator for a one-particle state on site :math:`i`
(:math:`j`) with :math:`i, j \in \{0, \ldots N-1\}`, and :math:`H_{ij}`
are matrix elements of the Hamiltonian. The three contributions belong
to system :math:`S`, leads :math:`L` and the system-lead coupling.
Static matrix elements of the Hamiltonian are set to :math:`H_{ii} = 1`
and :math:`H_{ij} = -1` for nearest neighbors. The time-dependent
perturbation should act only on onsite Hamiltonian element of the
leftmost site of the system (with site index zero)

.. math::


       H_{ii}(t) = H_{ii} + \delta_{i, 0} V(t),

and the time-dependent perturbation is supposed to have the form of a
smooth step function

.. math::


       V(t) = (1 + \textrm{erf}((t - t_0) / \tau)) / 2.

Our observable is the onsite electron density

.. math::


       n_i(t) = \langle \hat{c}^\dagger_i (t) \hat{c}_i (t) \rangle.

The many-body electron density can be calculated by averaging over all
occupied one-body states :math:`|\psi\rangle`

.. math::


       \langle \hat{c}^\dagger_i (t) \hat{c}_i (t) \rangle =  \sum_\alpha \int_{-\infty}^{E_\text{F}} \frac{d E}{2 \pi} \langle \psi_{\alpha E}(i, t)  |\hat{c}^\dagger_i \hat{c}_i | \psi_{\alpha E}(j, t) \rangle.

The time evolution of the onsite electron density is now calculated with
tkwant.

.. jupyter-execute::
    :hide-code:

    import warnings
    warnings.simplefilter('ignore')  # cache possibly performance warning

In order to use tkwant, we have to import two modules

.. jupyter-execute::

    import kwant
    import tkwant

For our example, a few additional modules are required

.. jupyter-execute::

    import numpy as np
    import matplotlib.pyplot as plt
    from scipy.special import erf

.. jupyter-execute::
    :hide-code:

    %matplotlib inline

To set up the discretized tight-binding system in practice, ``tkwant``
uses the same mechanism as ``kwant``. We therefore expect that the
reader is familiar with ``kwant`` already, especially its way to set up
a system. In the following, we construct a one dimensional system with
two semi-infinite leads attached on each site

.. jupyter-execute::

    def create_system(length):

        def onsite_potential(site, time):
            return 1 + v(time)

        # system building
        lat = kwant.lattice.square(a=1, norbs=1)
        syst = kwant.Builder()

        # central scattering region
        syst[(lat(x, 0) for x in range(length))] = 1
        syst[lat.neighbors()] = -1
        # time dependent onsite-potential V(t) at leftmost site
        syst[lat(0, 0)] = onsite_potential

        # add leads
        sym = kwant.TranslationalSymmetry((-1, 0))
        lead_left = kwant.Builder(sym)
        lead_left[lat(0, 0)] = 1
        lead_left[lat.neighbors()] = -1
        syst.attach_lead(lead_left)
        syst.attach_lead(lead_left.reversed())

        return syst

The time dependent
perturbation :math:`V(t)` is

.. jupyter-execute::

    def v(time):
        t0 = 20
        tau = 4
        return (1 + erf((time - t0) / tau)) / 2

As parameters for the simulation we set

.. jupyter-execute::

    tmax = 40
    length = 5

We construct the system and finalize it, in order to allow numerical
calculations.

.. jupyter-execute::

    syst = create_system(length).finalized()

We can plot the system to have a first look. The two attached leads on
the left and the right of the chain are highlighted in red. The
time-dependent site is the one with index zero.

.. jupyter-execute::

    kwant.plot(syst);

The time-dependent potential :math:`V(t)` that is added on the first
site has the form

.. jupyter-execute::

    times = np.linspace(0, tmax, 100)
    plt.plot(times, [v(t) for t in times])
    plt.xlabel(r'time $t$')
    plt.ylabel(r'time-dependent perturbation $V(t)$')
    plt.show()

We consider the two semi-infite leads to be non-interacting and at
thermal equilibrium. The occupation of the two leads is thus
characterized only by a temperature :math:`T` and a chemical potential
:math:`\mu`. By default, `tkwant` assumes a temperature :math:`T = 0`,
and a chemical potential :math:`\mu = 0`.
We can plot the dispersion of one lead in the first Brillouine zone
(blue, straight) and also the Fermi level (black, dashed). All states
below the dashed line will contribute to the dynamics.

.. jupyter-execute::

    chemical_potential = 0
    kwant.plotter.bands(syst.leads[0], show=False)
    plt.plot([-np.pi, np.pi], [chemical_potential] * 2, 'k--')
    plt.show()

Tkwant can directly use the observable in terms of the corresponding
kwant operator

.. jupyter-execute::

    density_operator = kwant.operator.Density(syst)

To perform the actual simulation, we first have to initialize the
many-body state. The evolve and evaluate methods of the state allow to
propagate all occupied one-body states :math:`|\psi\rangle` in time and
to evaluate the operator.

.. jupyter-execute::

    state = tkwant.manybody.State(syst, tmax=tmax)

    charge_density = []
    for time in times:
        state.evolve(time)
        density = state.evaluate(density_operator)
        charge_density.append(density)

We plot the time evolution of the onsite charge densities for the
different sites

.. jupyter-execute::

    charge_density = np.array(charge_density)
    charge_density -= charge_density[0]  # substract offset at initial time
    [plt.plot(times, charge_density[:, i], label='site ' + str(i)) for i in range(length)]
    plt.xlabel(r'time $t$')
    plt.ylabel(r'charge density $n$')
    plt.legend()
    plt.show()

The complete example code can be also found in :ref:`1d_wire_onsite`

Time dependent coupling elements
--------------------------------

In the above example, the time dependent perturbation modified the
onsite elements :math:`\hat{H}_{ii}` of the Hamiltonian. Now, we like to
modify coupling elements :math:`\hat{H}_{ij}` with :math:`i \neq j`, in
order to introduce a time dependent perturbation. Therefore, we include
a time dependent potential drop at a position :math:`i_b`, such that the
system Hamiltonian (the additional lead part is not shown here) becomes

.. math::


       \hat{H}(t) =\sum_{ij} \gamma_{ij} c^\dagger_i c_i + \sum_i w(t) \theta(i_b - i) c^\dagger_i c_i

:math:`\theta(x)` is the Heaviside function and :math:`w(t)` an
arbitrary function parametrizing the time-dependent perturbation that we
like to apply to the system. In this example we choose a Gaussian
function

.. math::


       w(t) = \theta(t) v_p e^{- 2 (t / \tau)^2}

where :math:`v_p` is some strenght and :math:`\tau` accounts for the
width of the pulse. Note the convention that the time-dependent
perturbation has to start after time :math:`t=0`. One can absorb the
effect of the time-dependent perturbation by a gauge transform. Defining
the integrated pulse

.. math::


       \phi(t) = \int_0^t dt' w(t') = A (1 + \textrm{erf}( t / \sigma)), \qquad A = \frac{v_p}{4} \sqrt{\frac{1}{2 \pi}}, \,\, \sigma =  \tau / \sqrt{2}

we just have to rewrite the coupling :math:`\gamma` between site
:math:`i_b` and :math:`i_b + 1` by the time dependent coupling
:math:`\gamma(t)`:

.. math::


       \gamma \rightarrow \gamma(t) = \gamma e^{- i \phi(t)}

In the following code we define the function :math:`\phi(t)` named
``gaussian`` and replace the coupling between site 0 and 1 by the time
dependent coupling

.. jupyter-execute::

    import cmath

    def make_system(a=1, gamma=1.0, W=10, L=30):

        lat = kwant.lattice.square(a=a, norbs=1)
        syst = kwant.Builder()

        def gaussian(time):
            t0 = 0
            A = 0.00157
            sigma = 24
            return A * (1 + erf((time - t0) / sigma))

        # time dependent coupling with gaussian pulse
        def coupling_nn(site1, site2, time):
            return - gamma * cmath.exp(- 1j * gaussian(time))

        #### Define the scattering region. ####
        syst[(lat(x, y) for x in range(L) for y in range(W))] = 4 * gamma
        syst[lat.neighbors()] = -gamma
        # time dependent coupling between two sites 0 and 1
        for y in range(W):
            syst[lat(0, y), lat(1, y)] = coupling_nn

        #### Define and attach the leads. ####
        # Construct the left lead.
        lead = kwant.Builder(kwant.TranslationalSymmetry((-a, 0)))
        lead[(lat(0, j) for j in range(W))] = 4 * gamma
        lead[lat.neighbors()] = -gamma

        # Attach the left lead and its reversed copy.
        syst.attach_lead(lead)
        syst.attach_lead(lead.reversed())

        return syst

    syst = make_system()

    kwant.plot(syst, site_color='k');

Time dependent couplings between lead and system
------------------------------------------------

The special case of a time dependent coupling between the sites at the
system-lead interface shown above can be written in more compact form.
We first defines a system as before, but without the time dependent
part.

.. jupyter-execute::

    def make_system(a=1, gamma=1.0, W=10, L=30):

        lat = kwant.lattice.square(a=a, norbs=1)
        syst = kwant.Builder()

        #### Define the scattering region. ####
        syst[(lat(x, y) for x in range(L) for y in range(W))] = 4 * gamma
        syst[lat.neighbors()] = -gamma

        #### Define and attach the leads. ####
        # Construct the left lead.
        lead = kwant.Builder(kwant.TranslationalSymmetry((-a, 0)))
        lead[(lat(0, j) for j in range(W))] = 4 * gamma
        lead[lat.neighbors()] = -gamma

        # Attach the left lead and its reversed copy.
        syst.attach_lead(lead)
        syst.attach_lead(lead.reversed())

        return syst

    syst = make_system()

The time dependent couplings are added by

.. jupyter-execute::

    def gaussian(time):
        t0 = 0
        A = 0.00157
        sigma = 24
        return A * (1 + erf((time - t0) / sigma))

    added_sites = tkwant.leads.add_voltage(syst, 0, gaussian)

In fact, the routine adds new sites at the system-lead interface and
modifies ``syst``. Note that ``syst`` must not be finalized. We can also
skip ``added_sites`` and call ``tkwant.leads.add_voltage`` without
return argument, if we are not interested in the added sites. The second
function argument of ``tkwant.leads.add_voltage`` corresponds to the
lead number, here ``0``, where the sites are injected. We can show the
new sites with time dependent couplings (in blue) if we plot the system.

.. jupyter-execute::

    interface_hoppings = [(a, b)
                          for b in added_sites
                          for a in syst.neighbors(b) if a not in added_sites]
    kwant.plot(syst, show=False,
               site_size=lambda s: 0.4 if s in added_sites else 0.25,
               hop_lw=lambda a, b: 0.3 if (a, b) in interface_hoppings else 0.1,
               site_color=lambda s: 'b' if s in added_sites else 'k',
               hop_color=lambda a, b: 'b' if (a, b) in interface_hoppings else 'k');

Note that in fact the system is not exactly the same as before due to
the additional sites, that were added. We could have constructed the
system with ``syst = make_system(L=29)`` to recover exactly the same
length as in the example before.

Examples
--------

:ref:`1d_wire_onsite`

References
----------

[1] B. Gaury, J. Weston, M. Santin, M. Houzet, C. Groth and X. Waintal,
`Numerical simulations of time-resolved quantum electronics 
<https://www.sciencedirect.com/science/article/pii/S0370157313003451?via%3Dihub>`__, Phys. Rep.
**534**, 1 (2014). `[arXiv] <https://arxiv.org/abs/1307.6419>`__
