:mod:`tkwant.operator` -- Energy related operators and observables
=========================================================================

.. module:: tkwant.operator

Helper Functions
----------------
.. autosummary::
   :toctree: generated/

   EnergySiteValidator

Observables
-----------
.. autosummary::
   :toctree: generated/

   EnergyCurrent
   EnergySource
   EnergyDensity
   LeadHeatCurrent
   EnergyCurrentDivergence
