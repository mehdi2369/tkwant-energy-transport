:orphan:

.. _closed_system:

Closed system
=============

Physics Background
------------------
Evolving single-particle states forward in time in a closed system,
and calculating the expectation value of the radial position operator.

**tkwant features highlighted**

-  Use of ``tkwant.onebody.WaveFunction`` to solve the time-dependent Schrödinger
   equation for a closed system.

The code can be also found in
:download:`closed_system.py <closed_system.py>`.

.. jupyter-execute::
    :hide-code:
    
    # prevent problems with figures from the first code
    import matplotlib.pyplot as plt
    %matplotlib inline


.. jupyter-execute:: closed_system.py

