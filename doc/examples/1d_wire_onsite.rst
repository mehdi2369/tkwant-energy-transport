:orphan:

.. _1d_wire_onsite:

1d wire with onsite potential
=============================

The code can be also found in
:download:`1d_wire_onsite.py <1d_wire_onsite.py>`.

.. jupyter-execute::
    :hide-code:
    
    # prevent problems with figures from the first code
    import matplotlib.pyplot as plt
    %matplotlib inline


.. jupyter-execute:: 1d_wire_onsite.py

