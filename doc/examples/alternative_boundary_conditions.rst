:orphan:

.. _alternative_boundary_conditions:

Alternative Boundary Conditions
===============================

**tkwant features highlighted**

- Selecting alternative boundary conditions to improve performance

The code can be also found in
:download:`alternative_boundary_conditions.py <alternative_boundary_conditions.py>`.

.. jupyter-execute::
    :hide-code:
    
    # prevent problems with figures
    import matplotlib.pyplot as plt
    %matplotlib inline

.. jupyter-execute:: alternative_boundary_conditions.py

