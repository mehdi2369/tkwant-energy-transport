:orphan:

.. _1d_wire:

1d wire
=======

In the following example, a one-dimensional quantum wire is exposed to a
Gaussian shaped potential drop that occurs in the center. We will evolve
the many-body state forward in time and calculate the onsite density on
each grid site.

**tkwant features highlighted**

-  Use of the adaptive solver ``tkwant.manybody.State()`` to solve the
   time-dependent many-body Schrödinger equation for an open system.
-  Refining the integration intervals and estimating the numerical error.
-  Use of MPI directives in order to perform plotting and printing only
   by rank zero.


The example can be found in
:download:`1d_wire.py <1d_wire.py>`.

.. literalinclude:: 1d_wire.py
   :language: python

**TODO**: change to a running example, this code takes too much time
