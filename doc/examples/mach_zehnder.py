from types import SimpleNamespace
from math import tanh, pi, sqrt, log, cos
import numpy as np
from scipy.special import erf
import functools as ft
from mpi4py import MPI
import matplotlib.pyplot as plt

import kwant
import kwant_spectrum
import tkwant
from tkwant import manybody


"""
    Sketch of the system
    --------------------

        +-----------------------------+
    0   #                             #  3
        #                             #
        +----+                   +----+
             |XXXXXXXXXXXXXXXXXXX| ----> central gate
        +----+                   +----+
    1   #                             #  2
        #                             #
        +-----------------------------+
"""


def am_master():
    return MPI.COMM_WORLD.rank == 0


def create_system(a, L, W):
    gamma = 1 / a**2
    Lreal = L * a
    Wreal = W * a

    # === injection filter and gate definitions ===
    def injection_filter(site, time, syst_args):
        Vb, xi, d = syst_args.Vb, syst_args.xi, syst_args.d
        x = site.pos[0] - Lreal / 2
        return 0.5 * Vb * (np.tanh((x + d) / xi) - np.tanh((x - d) / xi))

    def gate(site, time, syst_args):
        Vg = syst_args.Vg
        x = site.pos[0]
        xi = 20  # XXX do not change
        d = Lreal/2 - 150  # XXX tightly coupled to xi
        x = x - Lreal/2
        Vinf = 4  # XXX very high value
        return Vg + Vinf * (1 - 0.5*(tanh((x + d) / xi) - tanh((x - d) / xi)))

    # === onsite function definitions ===
    def onsite(site, time, syst_args):
        return 4 * gamma + injection_filter(site, time, syst_args)

    def onsite_under_gate(site, time, syst_args):
        return 4 * gamma + injection_filter(site, time, syst_args) + gate(site, time, syst_args)

    # === System Building ===
    lat = kwant.lattice.square(a=a, norbs=1)
    syst = kwant.Builder()

    # main part of system (upper and lower branches)
    syst[(lat(x, y) for x in range(L) for y in range(W))] = onsite
    syst[(lat(x, y) for x in range(L) for y in range(-(W+1), -1))] = onsite
    # sites under central gate -- joining upper and lower branches
    syst[(lat(x, -1) for x in range(int(100/a), L - int(100/a)))] = onsite_under_gate

    syst[lat.neighbors()] = -gamma

    sym = kwant.TranslationalSymmetry((-a, 0))
    lead_up = kwant.Builder(sym)
    lead_up[(lat(0, y) for y in range(W))] = 4*gamma
    lead_up[lat.neighbors()] = -gamma
    lead_down = kwant.Builder(sym)
    lead_down[(lat(0, y) for y in range(-(W+1), -1))] = 4*gamma
    lead_down[lat.neighbors()] = -gamma

    syst.attach_lead(lead_up)
    syst.attach_lead(lead_down)
    syst.attach_lead(lead_down.reversed())
    syst.attach_lead(lead_up.reversed())

    return syst, lat


# === system construction parameters ===
Wreal = 20
Lreal = 1200
a = 2  # discretisation (smaller is "better")

chemical_potential = 0.15  # Fermi energy

syst_args = SimpleNamespace()

# === central gate parameters ===
syst_args.Vg = 0.269

# === injection filter ===
syst_args.Vb = 0.11
syst_args.xi = 20
syst_args.d = Lreal / 2 - 70

# === pulse parameters ===
nbar = 2.0  # area under voltage pulse
syst_args.taup = 200
syst_args.t0 = 400
syst_args.Vp = 4 * (nbar / syst_args.taup) * sqrt(log(2) * pi)

# === simulation parameters ===
tmax = 6000

#############################

W = int(Wreal / a)
L = int(Lreal / a)
gamma = 1. / a**2

syst, lat = create_system(a, L, W)

# === Pulse definition ===
def gaussian(time, syst_args):
    Vp, t0, taup = syst_args.Vp, syst_args.t0, syst_args.taup
    sigma = taup / (2 * sqrt(log(2)))
    A = Vp * sigma * sqrt(np.pi) / 2
    return A * (1 + erf((time - t0) / sigma))

tkwant.leads.add_voltage(syst, 0, gaussian)
syst = syst.finalized()

kwant.plot(syst)

# === Observables definitions ===

in_hops = [(lat(51, y), lat(50, y)) for y in range(W)]
# current *after* the injection filter
I_in = kwant.operator.Current(syst, where=in_hops, sum=True)
# current in lead 2
lead2_hops = [(lat(L-1, y), lat(L-2, y)) for y in range(W)]
I_2 = kwant.operator.Current(syst, where=lead2_hops, sum=True)
# current in lead 3
lead3_hops = [(lat(L-1, y), lat(L-2, y)) for y in range(-(W + 1), -1)]
I_3 = kwant.operator.Current(syst, where=lead3_hops, sum=True)

# === tkwant simulation ===

kwant.plotter.bands(syst.leads[0], show=False)
_ = plt.plot([-pi, pi], [chemical_potential] * 2, 'k--')  # fermi energy

# we will only calculate the contribution from lead 0, under the
# assumption that the effect of the perturbation on the scattering
# states originating from other leads will be negligible
occupation = tkwant.manybody.lead_occupation(chemical_potential, bands=0)
occupations = [occupation, None, None, None]

spectra = kwant_spectrum.spectra(syst.leads)
Interval = ft.partial(manybody.Interval, order=15)
intervals = manybody.calc_intervals(spectra, occupations, interval_type=Interval)
intervals = manybody.split_intervals(intervals, 28)
boundaries = tkwant.leads.automatic_boundary(spectra, tmax)
tasks = manybody.calc_tasks(intervals, spectra, occupations)
psi_init = manybody.calc_initial_state(syst, tasks, boundaries,
                                                params={'syst_args': syst_args})

# do the actual tkwant simulation
solver = manybody.WaveFunction(psi_init, tasks)
times = np.arange(0, tmax, 5)

results = []
for time in times:
    solver.evolve(time)
    results.append((time, solver.evaluate(I_in), solver.evaluate(I_2), solver.evaluate(I_3)))
    if am_master():
        print(time)

if am_master():
    _, i1, i2, i3 = zip(*results)
    plt.plot(times, i1, label='lead0')
    plt.plot(times, i2, label='lead2')
    plt.plot(times, i3, label='lead3')
    plt.xlabel(r'time $t$')
    plt.ylabel(r'current $I$')
    plt.legend()
    plt.savefig('mach_zehnder_pulse.png')
    plt.show()
