import warnings
import numpy as np
import matplotlib.pyplot as plt
from scipy.special import erf

import kwant
import tkwant

warnings.simplefilter('ignore')  # cache possibly performance warning


def v(time, t0=20, tau=4):
    """Time dependent perturbation V(t)"""
    return (1 + erf((time - t0) / tau)) / 2


def create_system(length):

    def onsite_potential(site, time):
        """Time dependent onsite potential (static part + V(t))"""
        return 1 + v(time)

    # system building
    lat = kwant.lattice.square(a=1, norbs=1)
    syst = kwant.Builder()

    # central scattering region
    syst[(lat(x, 0) for x in range(length))] = 1
    syst[lat.neighbors()] = -1
    # time dependent onsite-potential at the leftmost site
    syst[lat(0, 0)] = onsite_potential

    # add leads
    sym = kwant.TranslationalSymmetry((-1, 0))
    lead_left = kwant.Builder(sym)
    lead_left[lat(0, 0)] = 1
    lead_left[lat.neighbors()] = -1
    syst.attach_lead(lead_left)
    syst.attach_lead(lead_left.reversed())

    return syst


def main():

    # parameters
    tmax = 40
    length = 5

    # create system
    syst = create_system(length).finalized()

    # plot the system and dispersion
    chemical_potential = 0
    kwant.plot(syst)
    kwant.plotter.bands(syst.leads[0], show=False)
    plt.plot([-np.pi, np.pi], [chemical_potential, chemical_potential], 'k--')
    plt.show()

    # plot the time-dependent perturbation
    times = np.linspace(0, tmax, 100)
    plt.plot(times, [v(t) for t in times])
    plt.xlabel(r'time $t$')
    plt.ylabel(r'time-dependent perturbation $V(t)$')
    plt.show()

    # define an observable
    density_operator = kwant.operator.Density(syst)

    # do the actual tkwant simulation
    solver = tkwant.manybody.State(syst, tmax=tmax)

    charge_density = []
    for time in times:
        solver.evolve(time)
        charge_density.append(solver.evaluate(density_operator))

    # plot the result
    charge_density = np.array(charge_density)
    charge_density -= charge_density[0]  # substract offset at initial time
    for site in range(length):
        plt.plot(times, charge_density[:, site], label='site {}'.format(site))
    plt.xlabel(r'time $t$')
    plt.ylabel(r'charge density $n$')
    plt.legend()
    plt.show()

if __name__ == '__main__':
    main()
