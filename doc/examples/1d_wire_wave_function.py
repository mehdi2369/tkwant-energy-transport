import cmath
import functools
import numpy as np
import matplotlib.pyplot as plt
from scipy.special import erf

import kwant
import tkwant
from tkwant import leads, manybody
import kwant_spectrum


def am_master():
    """Return true for the MPI master rank"""
    return tkwant.mpi.get_communicator().rank == 0


def create_system(L=1000):

    def gaussian(time, t0=10, A=1.57, sigma=24):
        return A * (1 + erf((time - t0) / sigma))

    # time dependent coupling with gaussian pulse
    def coupling_nn(site1, site2, time):
        return - cmath.exp(- 1j * gaussian(time))

    # system building
    lat = kwant.lattice.square(a=1, norbs=1)
    syst = kwant.Builder()

    # central scattering region
    syst[(lat(x, 0) for x in range(L))] = 1
    syst[lat.neighbors()] = -1
    # time dependent coupling between two sites in the center
    syst[lat(L//2-2, 0), lat(L//2-3, 0)] = coupling_nn

    # add leads
    sym = kwant.TranslationalSymmetry((-1, 0))
    lead_left = kwant.Builder(sym)
    lead_left[lat(0, 0)] = 1
    lead_left[lat.neighbors()] = -1
    syst.attach_lead(lead_left)
    syst.attach_lead(lead_left.reversed())

    return syst


def main():

    syst = create_system().finalized()
    sites = [site.pos[0] for site in syst.sites]
    density_operator = kwant.operator.Density(syst)

    occupation = manybody.lead_occupation()
    spectra = kwant_spectrum.spectra(syst.leads)
    boundaries = leads.automatic_boundary(spectra, tmax=400)
    Interval = functools.partial(manybody.Interval, quadrature='gausslegendre', order=10)
    intervals = manybody.calc_intervals(spectra, occupation, interval_type=Interval)
    intervals = manybody.split_intervals(intervals, 15)
    tasks = manybody.calc_tasks(intervals, spectra, occupation)
    psi_init = manybody.calc_initial_state(syst, tasks, boundaries)

    wave_function = manybody.WaveFunction(psi_init, tasks)

    density_0 = wave_function.evaluate(density_operator)

    for time in [100, 200, 300, 400]:
        wave_function.evolve(time=time)
        density_t = wave_function.evaluate(density_operator)
        if am_master():
            plt.plot(sites, density_t - density_0, label='time= ' + str(time))

    if am_master():
        plt.legend()
        plt.xlabel(r'site position $i$')
        plt.ylabel(r'charge density $n$')
        plt.show()

if __name__ == '__main__':
    main()
