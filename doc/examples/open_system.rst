:orphan:

.. _open_system:

Open system
===========

Physics Background
------------------
Evolving single-particle states forward in time in an open system,
and calculating the expectation value of the current.

**tkwant features highlighted**

-  Use of ``tkwant.leads.add_voltage`` to add time-dependence to leads.
-  Use of ``tkwant.onebody.WaveFunction`` to solve the time-dependent Schrödinger
   equation for an open system.

The code can be also found in 
:download:`open_system.py <open_system.py>`.

.. jupyter-execute::
    :hide-code:
    
    # prevent problems with figures from the first code
    import matplotlib.pyplot as plt
    %matplotlib inline


.. jupyter-execute:: open_system.py

