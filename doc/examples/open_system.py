from math import sin, pi
import numpy as np
import matplotlib
from matplotlib import pyplot as plt

import kwant
import tkwant


def make_system(a=1, t=1.0, radius=10, width=7):
    """Make a tight binding system on a single square lattice"""
    # `a` is the lattice constant and `t` the hopping integral
    # both set by default to 1 for simplicity.

    lat = kwant.lattice.square(a, norbs=1)
    syst = kwant.Builder()

    # Define the quantum dot
    def circle(pos):
        (x, y) = pos
        return x ** 2 + y ** 2 < radius ** 2

    syst[lat.shape(circle, (0, 0))] = 4 * t
    syst[lat.neighbors()] = -t

    lead = kwant.Builder(kwant.TranslationalSymmetry((-1, 0)))
    lead[(lat(0, j) for j in range(-(width-1)//2, width//2 + 1))] = 4 * t
    lead[lat.neighbors()] = -t
    syst.attach_lead(lead)
    syst.attach_lead(lead.reversed())

    return syst


def faraday_flux(time):
    return 0.1 * (time - 10 * sin(0.1 * time)) / 2


def main():

    # create a system andadd a time-dependent voltage to lead 0 -- this is
    # implemented by adding sites to the system at the interface with the lead
    # and multiplying the hoppings to these sites by exp(-1j * faraday_flux(time))
    syst = make_system()

    added_sites = tkwant.leads.add_voltage(syst, 0, faraday_flux)
    interface_hoppings = [(a, b)
                          for b in added_sites
                          for a in syst.neighbors(b) if a not in added_sites]

    fsyst = syst.finalized()

    current_operator = kwant.operator.Current(fsyst, where=interface_hoppings,
                                              sum=True)

    # create a time-dependent wavefunction that starts in a scattering state
    # originating from the left lead
    scattering_states = kwant.wave_function(fsyst, energy=1., params={'time': 0})
    psi_st = scattering_states(0)[0]

    tmax = 200 * pi
    times = np.arange(0, tmax)
    boundaries = [tkwant.leads.SimpleBoundary(tmax=tmax)] * len(syst.leads)
    psi = tkwant.onebody.WaveFunction.from_kwant(fsyst, psi_st, boundaries=boundaries,
                                                 energy=1.)

    # evolve forward in time, calculating the current
    current = []
    for time in times:
        psi.evolve(time)
        current.append(psi.evaluate(current_operator))

    # plot results
    plt.figure(figsize=(18, 7))

    gs = matplotlib.gridspec.GridSpec(2, 2)

    ax1 = plt.subplot(gs[0, 0])
    ax1.plot(times, current)
    ax1.set_ylabel('Current')

    ax2 = plt.subplot(gs[1, 0])
    ax2.plot(times, 0.5 * np.cos(0.1 * times))
    ax2.set_xlabel('Time')
    ax2.set_ylabel('Voltage')

    ax3 = plt.subplot(gs[:, 1])
    kwant.plot(syst,
               ax=ax3,
               site_size=lambda s: 0.4 if s in added_sites else 0.25,
               hop_lw=lambda a, b: 0.3 if (a, b) in interface_hoppings else 0.1,
               site_color=lambda s: 'r' if s in added_sites else 'b',
               hop_color=lambda a, b: 'r' if (a, b) in interface_hoppings else 'k',
               lead_color='grey', num_lead_cells=3)
    plt.show()

if __name__ == '__main__':
    main()
