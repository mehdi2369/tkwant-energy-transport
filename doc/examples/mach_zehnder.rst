:orphan:

.. _mach_zehnder:

Flying qubit interferometer
===========================

**Physics Background**

Applying a voltage pulses to the Mach Zehnder interferometer, see Ref.
`[1] <#references>`__.

The code can be also found in
:download:`mach_zehnder.py <mach_zehnder.py>`.

.. literalinclude:: mach_zehnder.py
   :language: python

**TODO**: change to a running example, this code takes too much time


Note that above code takes long time to run, so we only show the result
in the following.

|image0| 

Figure: Result of the tkwant simulation of the Mach Zehnder interferometer.

.. |image0| image:: mach_zehnder_pulse.png

References
----------

[1] J. Weston and X. Waintal, `Towards realistic time-resolved simulations of quantum
devices <https://link.springer.com/article/10.1007%2Fs10825-016-0855-9>`__, J. Comput. Electron. **15**, 1148 (2016).
`[arXiv] <https://arxiv.org/abs/1604.01198>`__

