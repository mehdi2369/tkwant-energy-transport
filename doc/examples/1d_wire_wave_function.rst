:orphan:

.. _1d_wire_wave_function:

1d wire - wave function approach
================================

In the following example, a one-dimensional quantum wire is exposed to a
Gaussian shaped potential drop that occurs in the center. We will evolve
the many-body state forward in time and calculate the onsite density on
each grid site. This example is similar to
:ref:`1d_wire`, but the manybody wavefunction is built up from scratch.

**tkwant features highlighted**

-  Use of the non-adaptive solver ``manybody.WaveFunction()`` to solve the
   time-dependent many-body Schrödinger equation for an open system.
-  Use of MPI directives in order to perform plotting and printing only
   by rank zero.


The example can be found in
:download:`1d_wire_wave_function.py <1d_wire_wave_function.py>`.

.. literalinclude:: 1d_wire_wave_function.py
   :language: python

**TODO**: change to a running example, this code takes too much time
