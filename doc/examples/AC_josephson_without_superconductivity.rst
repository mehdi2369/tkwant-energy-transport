:orphan:

.. _AC_josephson_without_superconductivity:

AC Josephson effect without superconductivity
=============================================

**Physics Background**

Applying voltage pulses to Fabry-Perot cavities to illustrate dynamic
control of interference, see Ref. `[1] <#references>`__.

The code can be also found in
:download:`AC_josephson_without_superconductivity.py <AC_josephson_without_superconductivity.py>`.

.. jupyter-execute::
    :hide-code:
    
    # prevent problems with figures from the first code
    import matplotlib.pyplot as plt
    %matplotlib inline

.. jupyter-execute:: AC_josephson_without_superconductivity.py

References
----------

[1] B. Gaury, J. Weston, X. Waintal,
`The a.c. Josephson effect without superconductivity 
<https://www.nature.com/articles/ncomms7524>`__, 
Nat. Commun. **6**, 6524 (2015).
`[arXiv] <https://arxiv.org/abs/1407.3911>`__

