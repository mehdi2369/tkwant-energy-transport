:orphan:

.. _quantum_dot:

Quantum dot
===========

**tkwant features highlighted**
- Calculation of energy quantities: current, density, source, heat current

The code can be also found in
:download:`quantum_dot.py <quantum_dot.py>`.

.. jupyter-execute::
    :hide-code:
    
    # prevent problems with figures from the first code
    import matplotlib.pyplot as plt
    %matplotlib inline


.. jupyter-execute:: quantum_dot.py
